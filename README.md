Include role docker   

- name: docker   
  src: https://gitlab.com/library585605/ansible_roles/docker_install.git  
  scm: git   
  version: main    

 
Playbook Example      
   
- hosts:    
  gather_facts: true   
  become: true   
  roles:   
    - docker   
   